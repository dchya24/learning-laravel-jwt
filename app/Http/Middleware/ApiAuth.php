<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = base64_decode($request->token);

        if(!$request->token){
            return response()->json([
                'message' => 'unauthorized'
            ], 401);
        }

        $check = JWTAuth::authenticate($token);

        // if ($token = $this->auth->setRequest($request)->getToken()) {
            // return $token;
            // try {
            //     $this->auth->authenticate($token);
            // } catch (\Exception $e) {
            //     unset($e);
            // }
        // }

        return $next($request);
    }
}
