<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'api'], function () {
    Route::post('register', 'RegisterController@register');
    Route::post('login', 'LoginController@login');

    Route::group(['middleware' => ['api.auth'] ] , function() {
        Route::get('user', function(Request $request) {
            return auth()->user();
        });
        Route::post('user/edit', function(Request $request) {
            return $request->all();
        });
    });
});
